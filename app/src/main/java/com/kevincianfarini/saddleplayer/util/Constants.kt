package com.kevincianfarini.saddleplayer.util

object Constants {

    const val LAST_FM_API_KEY = "77fbaa759e661e9ae7c585e9081830ca"
    const val LAST_FM_API_ROOT = "https://ws.audioscrobbler.com/"

    const val SADDLE_API_ROOT = "https://4277597f.ngrok.io/api/"

}