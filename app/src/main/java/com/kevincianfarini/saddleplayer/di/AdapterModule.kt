package com.kevincianfarini.saddleplayer.di

import com.kevincianfarini.saddleplayer.data.local.MusicDatabase
import com.kevincianfarini.saddleplayer.data.local.dao.AlbumDao
import com.kevincianfarini.saddleplayer.data.local.dao.ArtistDao
import com.kevincianfarini.saddleplayer.data.local.dao.TrackDao
import com.kevincianfarini.saddleplayer.data.network.lastfm.LastFmAPIManager
import com.kevincianfarini.saddleplayer.feature.albumlist.adapter.AlbumDelegateAdapter
import com.kevincianfarini.saddleplayer.feature.artistlist.adapter.ArtistDelegateAdapter
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewTypeAdapter
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewTypeConstant
import com.kevincianfarini.saddleplayer.feature.tracklist.adapter.TrackDelegateAdapter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module class AdapterModule {

    @Provides
    fun providesDelegateAdapters(db: MusicDatabase) = ViewTypeAdapter(mapOf(
            ViewTypeConstant.ARTIST to ArtistDelegateAdapter(db),
            ViewTypeConstant.ALBUM to AlbumDelegateAdapter(db),
            ViewTypeConstant.TRACK to TrackDelegateAdapter(db)
    ))

}