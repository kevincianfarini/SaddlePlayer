package com.kevincianfarini.saddleplayer.di

import com.kevincianfarini.saddleplayer.SaddleApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: SaddleApplication) {

    @Singleton @Provides
    fun providesApp() = app

}