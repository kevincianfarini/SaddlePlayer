package com.kevincianfarini.saddleplayer.di

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.kevincianfarini.saddleplayer.data.network.lastfm.LastFmAPI
import com.kevincianfarini.saddleplayer.data.network.lastfm.LastFmAPIManager
import com.kevincianfarini.saddleplayer.data.network.saddle.SaddleAPI
import com.kevincianfarini.saddleplayer.data.network.saddle.SaddleAPIManager
import com.kevincianfarini.saddleplayer.util.Constants
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module class NetworkModule {

    @Provides @Singleton
    fun providesLastFmAPI(): LastFmAPI = Retrofit.Builder()
            .baseUrl(Constants.LAST_FM_API_ROOT)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(LastFmAPI::class.java)

    @Provides @Singleton
    fun providesLastFmAPIManager(api: LastFmAPI) = LastFmAPIManager(api)

    @Provides @Singleton
    fun providesSaddleAPI(): SaddleAPI = Retrofit.Builder()
            .baseUrl(Constants.SADDLE_API_ROOT)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(SaddleAPI::class.java)

    @Provides @Singleton
    fun providesSaddleAPIManager(api: SaddleAPI): SaddleAPIManager = SaddleAPIManager(api)
}