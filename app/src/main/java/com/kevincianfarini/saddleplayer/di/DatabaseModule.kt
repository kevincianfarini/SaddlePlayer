package com.kevincianfarini.saddleplayer.di

import com.kevincianfarini.saddleplayer.SaddleApplication
import com.kevincianfarini.saddleplayer.data.local.MusicDatabase
import com.kevincianfarini.saddleplayer.data.local.dao.AlbumDao
import com.kevincianfarini.saddleplayer.data.local.dao.ArtistDao
import com.kevincianfarini.saddleplayer.data.local.dao.TrackDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module class DatabaseModule {

    @Singleton @Provides
    fun providesDatabase(app: SaddleApplication): MusicDatabase = MusicDatabase.getInstance(app.applicationContext)

    @Provides @Singleton
    fun providesArtistDao(db: MusicDatabase): ArtistDao = db.artistDao

    @Provides @Singleton
    fun providesAlbumDao(db: MusicDatabase): AlbumDao = db.albumDao

    @Provides @Singleton
    fun providesTrackDao(db: MusicDatabase): TrackDao = db.trackDao

}