package com.kevincianfarini.saddleplayer.di

import com.kevincianfarini.saddleplayer.data.local.MusicDatabase
import com.kevincianfarini.saddleplayer.data.local.dao.AlbumDao
import com.kevincianfarini.saddleplayer.data.local.dao.ArtistDao
import com.kevincianfarini.saddleplayer.data.local.dao.TrackDao
import com.kevincianfarini.saddleplayer.data.local.entity.Album
import com.kevincianfarini.saddleplayer.data.local.entity.Artist
import com.kevincianfarini.saddleplayer.data.local.entity.Track
import com.kevincianfarini.saddleplayer.data.network.lastfm.LastFmAPIManager
import com.kevincianfarini.saddleplayer.data.network.saddle.SaddleAPIManager
import com.kevincianfarini.saddleplayer.feature.albumlist.viewmodel.AlbumListViewModel
import com.kevincianfarini.saddleplayer.feature.artistlist.viewmodel.ArtistListViewModel
import com.kevincianfarini.saddleplayer.feature.base.viewmodel.ListViewModel
import com.kevincianfarini.saddleplayer.feature.main.MainViewModel
import com.kevincianfarini.saddleplayer.feature.tracklist.viewmodel.TrackListViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module class ViewModelModule {

    @Provides @Singleton
    fun providesArtistListViewModel(dao: ArtistDao): ListViewModel<Artist> = ArtistListViewModel(dao)

    @Provides @Singleton
    fun providesAlbumListViewModel(dao: AlbumDao): ListViewModel<Album> = AlbumListViewModel(dao)

    @Provides @Singleton
    fun providesTrackListViewModel(dao: TrackDao): ListViewModel<Track> = TrackListViewModel(dao)

    @Provides @Singleton
    fun providesMainViewModel(api: SaddleAPIManager, db: MusicDatabase, lastFm: LastFmAPIManager) = MainViewModel(api, db, lastFm)

}