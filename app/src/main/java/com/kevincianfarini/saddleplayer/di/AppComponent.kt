package com.kevincianfarini.saddleplayer.di

import com.kevincianfarini.saddleplayer.feature.main.MainActivity
import com.kevincianfarini.saddleplayer.feature.albumlist.view.AlbumListFragment
import com.kevincianfarini.saddleplayer.feature.artistlist.view.ArtistListFragment
import com.kevincianfarini.saddleplayer.feature.tracklist.view.TrackListFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    DatabaseModule::class,
    NetworkModule::class,
    ViewModelModule::class,
    AdapterModule::class
])
interface AppComponent {

    fun inject(activity: MainActivity)
    fun inject(fragment: ArtistListFragment)
    fun inject(fragment: AlbumListFragment)
    fun inject(fragment: TrackListFragment)
}