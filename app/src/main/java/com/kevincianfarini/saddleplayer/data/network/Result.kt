package com.kevincianfarini.saddleplayer.data.network

sealed class Result<out T : Any>

class Success<out T : Any>(val data: T) : Result<T>()

class Failure(val error: Throwable) : Result<Nothing>()
