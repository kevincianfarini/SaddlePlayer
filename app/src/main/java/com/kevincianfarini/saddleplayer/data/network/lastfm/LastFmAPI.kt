package com.kevincianfarini.saddleplayer.data.network.lastfm

import com.kevincianfarini.saddleplayer.util.Constants
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface LastFmAPI {

    @GET("2.0")
    fun getArtistInfo(
            @Query("artist") artist: String,
            @Query("method") method: String = "artist.getInfo",
            @Query("api_key") apiKey: String = Constants.LAST_FM_API_KEY,
            @Query("format") format: String = "json"
    ): Deferred<Response<Map<Any?, Any?>>>

    @GET("2.0")
    fun getAlbumInfo(
            @Query("artist") artist: String,
            @Query("album") album: String,
            @Query("method") method: String = "album.getInfo",
            @Query("api_key") apiKey: String = Constants.LAST_FM_API_KEY,
            @Query("format") format: String = "json"
    ): Deferred<Response<Map<Any?, Any?>>>

}
