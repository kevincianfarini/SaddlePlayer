package com.kevincianfarini.saddleplayer.data.network.saddle

import com.kevincianfarini.saddleplayer.data.network.Failure
import com.kevincianfarini.saddleplayer.data.network.Result
import com.kevincianfarini.saddleplayer.data.network.Success
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.produce
import retrofit2.Response
import javax.inject.Singleton

@Singleton class SaddleAPIManager(private val api: SaddleAPI) {

    private suspend fun <T> getPaginatedData(response: Deferred<Response<PaginatedAPIResponse<T>>>): Result<PaginatedAPIResponse<T>> {
        val res = try {
            response.await()
        } catch (e: Exception) {
            return Failure(e)
        }

        if (res.isSuccessful) {
            val body = res.body()
            return body?.let {
                Success(body)
            } ?: Failure(Exception("Error parsing response"))
        }
        return Failure(Exception(res.message()))
    }

    private fun <T> buildResultChannel(lastupdated: Long? = null, request: suspend (Long?, Int?) -> Result<PaginatedAPIResponse<T>>) = GlobalScope.produce<List<T>>(Dispatchers.IO) {
        var nextPage: Int? = null
        do {
            val result = this.async { request(lastupdated, nextPage) }.await()
            when (result) {
                is Success -> {
                    send(result.data.results)
                    nextPage = result.data.next
                }
                is Failure -> throw result.error
            }
        } while (nextPage != null)
    }

    fun getArtists(lastUpdated: Long? = null) = buildResultChannel(lastUpdated) { updated: Long?, nextPage: Int? ->
        this.getPaginatedData(api.getArtists(updated, nextPage))
    }

    fun getAlbums(lastUpdated: Long? = null) = buildResultChannel(lastUpdated) { updated: Long?, nextPage: Int? ->
        this.getPaginatedData(api.getAlbums(updated, nextPage))
    }

    fun getTracks(lastUpdated: Long? = null) = buildResultChannel(lastUpdated) { updated: Long?, nextPage: Int? ->
        this.getPaginatedData(api.getTracks(updated, nextPage))
    }
}