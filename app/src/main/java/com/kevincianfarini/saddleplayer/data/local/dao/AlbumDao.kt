package com.kevincianfarini.saddleplayer.data.local.dao

import android.arch.paging.DataSource
import android.arch.persistence.room.*
import com.kevincianfarini.saddleplayer.data.local.entity.Album

@Dao interface AlbumDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg album: Album)

    @Update fun update(album: Album)

    @Query("SELECT * FROM Album ORDER BY name ASC")
    fun getAll(): DataSource.Factory<Int, Album>

    @Query("SELECT * FROM Album WHERE id=:albumId")
    fun get(albumId: Long): Album

}