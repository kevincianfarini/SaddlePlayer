package com.kevincianfarini.saddleplayer.data.network.saddle

data class PaginatedAPIResponse<out T>(
        val next: Int?,
        val count: Int,
        val results: List<T>
)