package com.kevincianfarini.saddleplayer.data.network.lastfm

import com.kevincianfarini.saddleplayer.data.local.entity.Album
import com.kevincianfarini.saddleplayer.data.local.entity.Artist
import com.kevincianfarini.saddleplayer.data.network.Failure
import com.kevincianfarini.saddleplayer.data.network.Result
import com.kevincianfarini.saddleplayer.data.network.Success
import com.kevincianfarini.saddleplayer.util.iterator
import kotlinx.coroutines.Deferred
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import javax.inject.Singleton

@Singleton
class LastFmAPIManager(private val api: LastFmAPI) {

    suspend fun getImageUrl(artist: Artist): Result<String> = getImageUrl("artist") {
        api.getArtistInfo(artist.name)
    }

    suspend fun getImageUrl(artist: Artist, album: Album): Result<String> = getImageUrl("album") {
        api.getAlbumInfo(artist.name, album.name)
    }

    private suspend fun getImageUrl(type: String, request: (() -> Deferred<Response<Map<Any?, Any?>>>)): Result<String> {
        val response = try {
            request.invoke().await()
        } catch (e: Exception) {
            return Failure(e)
        }

        if (response.isSuccessful) {
            val json = JSONObject(response.body())
            return json.let {
                return try {
                    Success(getLargestImageUrl(json.getJSONObject(type).getJSONArray("image")))
                } catch (e: JSONException) {
                    Failure(e)
                }
            }
        }
        return Failure(Exception(response.message()))
    }

    private fun getLargestImageUrl(images: JSONArray): String {
        val map = mapOf(
                "" to 0,
                "small" to 1,
                "medium" to 2,
                "large" to 3,
                "extralarge" to 4,
                "mega" to 5
        )

        var ret: Pair<Int, String>? = null
        for (image: JSONObject in images) {
            val imageScore = map[image.getString("size")] ?: 0

            if (ret == null) {
                ret = Pair(imageScore, image.getString("#text"))
            }

            val (retScore, _) = ret
            if (imageScore > retScore) {
                ret = Pair(imageScore, image.getString("#text"))
            }
        }

        return ret?.second ?: ""
    }

}
