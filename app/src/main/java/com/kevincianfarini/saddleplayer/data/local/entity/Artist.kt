package com.kevincianfarini.saddleplayer.data.local.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewType
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewTypeConstant

@Entity(tableName = "Artist")
data class Artist(
        @PrimaryKey val id: Long,
        val name: String,
        var imageUrl: String? = null
) : ViewType {
    override val viewType get() = ViewTypeConstant.ARTIST

    override fun areItemsSame(other: ViewType?) = other is Artist && other.id == this.id
}