package com.kevincianfarini.saddleplayer.data.local.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewType
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewTypeConstant

@Entity(
        tableName = "Track",
        foreignKeys = [
            (ForeignKey(
                entity = Album::class,
                parentColumns = ["id"],
                childColumns = ["album"]
        ))
        ]
)
data class Track(
        @PrimaryKey val id: Long,
        val album: Long, /* Foreign Key */
        val duration: Int,
        val name: String,
        val artist: String,
        val trackNumber: Int?
) : ViewType {
    override val viewType get() = ViewTypeConstant.TRACK

    override fun areItemsSame(other: ViewType?) = other is Track && other.id == this.id
}
