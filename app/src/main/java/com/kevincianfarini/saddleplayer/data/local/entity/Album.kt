package com.kevincianfarini.saddleplayer.data.local.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewType
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewTypeConstant

@Entity(
        tableName = "Album",
        foreignKeys = [
            ForeignKey(
                    entity = Artist::class,
                    parentColumns = ["id"],
                    childColumns = ["artist"]
            )
        ]
)
data class Album(
        @PrimaryKey val id: Long,
        val name: String,
        val duration: Int,
        val artist: Long /* Foreign Key */,
        var imageUrl: String? = null
) : ViewType {
    override val viewType get() = ViewTypeConstant.ALBUM

    override fun areItemsSame(other: ViewType?) = other is Album && other.id == this.id
}
