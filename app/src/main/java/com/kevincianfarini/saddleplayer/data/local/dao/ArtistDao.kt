package com.kevincianfarini.saddleplayer.data.local.dao

import android.arch.paging.DataSource
import android.arch.persistence.room.*
import com.kevincianfarini.saddleplayer.data.local.entity.Artist

@Dao interface ArtistDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg artist: Artist)

    @Update fun update(artist: Artist)

    @Query("SELECT * FROM Artist ORDER BY name ASC")
    fun getAll(): DataSource.Factory<Int, Artist>

    @Query("SELECT * FROM Artist WHERE id=:artistId")
    fun get(artistId: Long): Artist

    @Query(
            """
                SELECT COUNT(*)
                FROM Track AS a
                INNER JOIN Album As b ON a.album=b.id
                INNER JOIN Artist AS c ON b.artist=c.id
                WHERE c.id=:artistId
            """
    )
    fun trackCount(artistId: Long): Int

    @Query("SELECT COUNT(*) FROM Album AS a INNER JOIN Artist AS b ON a.artist=b.id WHERE b.id=:artistId")
    fun albumCount(artistId: Long): Int

}