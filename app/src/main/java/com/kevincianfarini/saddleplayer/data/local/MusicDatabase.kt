package com.kevincianfarini.saddleplayer.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.kevincianfarini.saddleplayer.data.local.dao.AlbumDao
import com.kevincianfarini.saddleplayer.data.local.dao.ArtistDao
import com.kevincianfarini.saddleplayer.data.local.dao.TrackDao
import com.kevincianfarini.saddleplayer.data.local.entity.Album
import com.kevincianfarini.saddleplayer.data.local.entity.Artist
import com.kevincianfarini.saddleplayer.data.local.entity.Track

@Database(entities = [Artist::class, Album::class, Track::class], version = 1)
abstract class MusicDatabase : RoomDatabase() {

    abstract val artistDao: ArtistDao
    abstract val albumDao: AlbumDao
    abstract val trackDao: TrackDao

    companion object {
        fun getInstance(context: Context): MusicDatabase = Room.databaseBuilder(
                context.applicationContext,
                MusicDatabase::class.java,
                "music.db"
        ).build()
    }

}