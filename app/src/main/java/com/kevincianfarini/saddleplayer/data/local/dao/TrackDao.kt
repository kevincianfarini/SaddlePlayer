package com.kevincianfarini.saddleplayer.data.local.dao

import android.arch.paging.DataSource
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.kevincianfarini.saddleplayer.data.local.entity.Album
import com.kevincianfarini.saddleplayer.data.local.entity.Track

@Dao interface TrackDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg items: Track)

    @Query("SELECT * FROM Track ORDER BY name ASC")
    fun getAll(): DataSource.Factory<Int, Track>

}