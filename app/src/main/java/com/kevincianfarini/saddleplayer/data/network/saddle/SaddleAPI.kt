package com.kevincianfarini.saddleplayer.data.network.saddle

import com.kevincianfarini.saddleplayer.data.local.entity.Album
import com.kevincianfarini.saddleplayer.data.local.entity.Artist
import com.kevincianfarini.saddleplayer.data.local.entity.Track
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SaddleAPI {

    @GET("tracks/")
    fun getTracks(@Query("last_updated") lastUpdated: Long? = null, @Query("page") page: Int? = null): Deferred<Response<PaginatedAPIResponse<Track>>>

    @GET("albums/")
    fun getAlbums(@Query("last_updated") lastUpdated: Long? = null, @Query("page") page: Int? = null): Deferred<Response<PaginatedAPIResponse<Album>>>

    @GET("artists/")
    fun getArtists(@Query("last_updated") lastUpdated: Long? = null, @Query("page") page: Int? = null): Deferred<Response<PaginatedAPIResponse<Artist>>>


}