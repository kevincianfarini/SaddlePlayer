package com.kevincianfarini.saddleplayer

import android.app.Application
import com.kevincianfarini.saddleplayer.di.AppComponent
import com.kevincianfarini.saddleplayer.di.AppModule
import com.kevincianfarini.saddleplayer.di.DaggerAppComponent

class SaddleApplication : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

}