package com.kevincianfarini.saddleplayer.feature.artistlist.view

import com.kevincianfarini.saddleplayer.data.local.entity.Artist
import com.kevincianfarini.saddleplayer.di.AppComponent
import com.kevincianfarini.saddleplayer.feature.artistlist.adapter.ArtistDelegateAdapter
import com.kevincianfarini.saddleplayer.feature.base.adapter.LayoutManagerType
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewTypeConstant
import com.kevincianfarini.saddleplayer.feature.base.view.ListFragment
import com.kevincianfarini.saddleplayer.feature.navigation.view.TabFragment

class ArtistListFragment : ListFragment<Artist>(), TabFragment {

    override val layoutManagerType = LayoutManagerType.LINEAR
    override val title: String = "Artists"

    override fun injectDependencies(component: AppComponent) {
        component.inject(this)
    }
}