package com.kevincianfarini.saddleplayer.feature.base.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PagedList
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewType

interface ListViewModel<T : ViewType> {

    val data: LiveData<PagedList<T>>
    val error: MutableLiveData<Throwable>

}