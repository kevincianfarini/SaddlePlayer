package com.kevincianfarini.saddleplayer.feature.tracklist.view

import com.kevincianfarini.saddleplayer.data.local.entity.Track
import com.kevincianfarini.saddleplayer.di.AppComponent
import com.kevincianfarini.saddleplayer.feature.base.adapter.LayoutManagerType
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewTypeConstant
import com.kevincianfarini.saddleplayer.feature.base.view.ListFragment
import com.kevincianfarini.saddleplayer.feature.navigation.view.TabFragment
import com.kevincianfarini.saddleplayer.feature.tracklist.adapter.TrackDelegateAdapter

class TrackListFragment : ListFragment<Track>(), TabFragment {

    override val layoutManagerType = LayoutManagerType.LINEAR
    override val title = "Tracks"

    override fun injectDependencies(component: AppComponent) {
        component.inject(this)
    }

}