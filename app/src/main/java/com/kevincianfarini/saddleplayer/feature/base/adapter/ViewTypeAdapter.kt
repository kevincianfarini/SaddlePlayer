package com.kevincianfarini.saddleplayer.feature.base.adapter

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.view.ViewGroup
import java.lang.IllegalArgumentException

class ViewTypeAdapter(adapters: Map<ViewTypeConstant, ViewTypeDelegateAdapter>) : PagedListAdapter<ViewType, CoroutineViewHolder>(DIFF_CALLBACK) {

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ViewType>() {
            override fun areItemsTheSame(oldItem: ViewType?, newItem: ViewType?): Boolean = oldItem?.areItemsSame(newItem)
                    ?: false

            override fun areContentsTheSame(oldItem: ViewType?, newItem: ViewType?): Boolean = oldItem == newItem
        }
    }

    private val delegateAdapters: Map<Int, ViewTypeDelegateAdapter> = adapters.mapKeys {
        it.key.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoroutineViewHolder =
            delegateAdapters[viewType]?.onCreateViewHolder(parent)
                    ?: throw IllegalArgumentException("viewType $viewType is not a delegate adapter")

    override fun onBindViewHolder(holder: CoroutineViewHolder, position: Int) {
        getItem(position)?.let {
            delegateAdapters[getItemViewType(position)]?.onBindViewHolder(holder, it)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position)?.viewType?.ordinal ?: 0
    }
}
