package com.kevincianfarini.saddleplayer.feature.albumlist.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.kevincianfarini.saddleplayer.R
import com.kevincianfarini.saddleplayer.data.local.MusicDatabase
import com.kevincianfarini.saddleplayer.data.local.entity.Album
import com.kevincianfarini.saddleplayer.feature.base.adapter.CoroutineViewHolder
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewType
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewTypeDelegateAdapter
import com.kevincianfarini.saddleplayer.util.inflate
import com.kevincianfarini.saddleplayer.util.load
import kotlinx.android.synthetic.main.list_item_album.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class AlbumDelegateAdapter(private val db: MusicDatabase) : ViewTypeDelegateAdapter {


    override fun onCreateViewHolder(parent: ViewGroup): CoroutineViewHolder = AlbumViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        (holder as AlbumViewHolder).bind(item as Album)
    }

    private inner class AlbumViewHolder(parent: ViewGroup) : CoroutineViewHolder(parent.inflate(R.layout.list_item_album)) {

        fun bind(album: Album) = with(itemView) {
            this@AlbumViewHolder.launch(Dispatchers.Main) {
                val artist = this.async(Dispatchers.IO) { db.artistDao.get(album.artist) }

                albumName.text = album.name
                albumArtistName.text = artist.await().name
                albumImage.load(
                        album.imageUrl ?: "",
                        placeholder = R.drawable.default_album_art
                )
            }
        }
    }
}