package com.kevincianfarini.saddleplayer.feature.base.view

import android.arch.lifecycle.Observer
import android.arch.paging.PagedList
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kevincianfarini.saddleplayer.feature.main.MainActivity
import com.kevincianfarini.saddleplayer.R
import com.kevincianfarini.saddleplayer.feature.base.adapter.*
import com.kevincianfarini.saddleplayer.feature.base.viewmodel.ListViewModel
import com.kevincianfarini.saddleplayer.feature.navigation.view.TabFragment
import com.kevincianfarini.saddleplayer.util.inflate
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

abstract class ListFragment<T : ViewType> : BaseFragment(), TabFragment {

    abstract val layoutManagerType: LayoutManagerType
    @Inject lateinit var viewModel: ListViewModel<T>
    @Inject lateinit var adapter: ViewTypeAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            container?.inflate(R.layout.fragment_list)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refresh.apply {
            setOnRefreshListener {
                (activity as MainActivity).refreshData(null) {
                    isRefreshing = false
                }
            }
        }

        list.apply {
            setHasFixedSize(true)
            layoutManager = when(layoutManagerType) {
                LayoutManagerType.GRID_2 -> GridLayoutManager(context, 2)
                LayoutManagerType.LINEAR -> LinearLayoutManager(context)
            }

            if (adapter == null) {
                adapter = this@ListFragment.adapter
            }

            this.setRecyclerListener {
                (it as CoroutineViewHolder).onRecycled()
            }
        }

        viewModel.run {
            data.observe(this@ListFragment, Observer {
                this@ListFragment.adapter.submitList(it as PagedList<ViewType>)
            })

            error.observe(this@ListFragment, Observer {
                Snackbar.make(list, it?.message ?: "", Snackbar.LENGTH_LONG).show()
            })
        }
    }
}