package com.kevincianfarini.saddleplayer.feature.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.kevincianfarini.saddleplayer.data.local.MusicDatabase
import com.kevincianfarini.saddleplayer.data.network.Success
import com.kevincianfarini.saddleplayer.data.network.lastfm.LastFmAPIManager
import com.kevincianfarini.saddleplayer.data.network.saddle.SaddleAPIManager
import kotlinx.coroutines.channels.consumeEach
import java.lang.Exception

class MainViewModel(
        private val api: SaddleAPIManager,
        private val db: MusicDatabase,
        private val lastFm: LastFmAPIManager
) : ViewModel() {

    private val _error: MutableLiveData<Throwable> = MutableLiveData()

    val error: LiveData<Throwable>
        get() = _error

    suspend fun refreshData(lastUpdated: Long?) {
        try {
            api.getArtists(lastUpdated).consumeEach {
                it.forEach { artist ->
                    val result = lastFm.getImageUrl(artist)
                    if (result is Success) {
                        artist.imageUrl = result.data
                    }
                    db.artistDao.insert(artist)
                }
            }

            api.getAlbums(lastUpdated).consumeEach {
                it.forEach { album ->
                    val result = lastFm.getImageUrl(
                            db.artistDao.get(album.artist),
                            album
                    )
                    if (result is Success) {
                        album.imageUrl = result.data
                    }
                    db.albumDao.insert(album)
                }
            }

            api.getTracks(lastUpdated).consumeEach {
                db.trackDao.insert(*it.toTypedArray())
            }
        } catch (e: Exception) {
            _error.postValue(e)
        }
    }

}