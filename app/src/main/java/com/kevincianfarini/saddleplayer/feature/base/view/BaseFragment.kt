package com.kevincianfarini.saddleplayer.feature.base.view

import android.os.Bundle
import android.support.v4.app.Fragment
import com.kevincianfarini.saddleplayer.SaddleApplication
import com.kevincianfarini.saddleplayer.di.AppComponent

abstract class BaseFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies((activity?.application as SaddleApplication).appComponent)
    }

    abstract fun injectDependencies(component: AppComponent)

}
