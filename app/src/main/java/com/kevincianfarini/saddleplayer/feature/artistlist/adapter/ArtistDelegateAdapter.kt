package com.kevincianfarini.saddleplayer.feature.artistlist.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.kevincianfarini.saddleplayer.R
import com.kevincianfarini.saddleplayer.data.local.MusicDatabase
import com.kevincianfarini.saddleplayer.data.local.entity.Artist
import com.kevincianfarini.saddleplayer.feature.base.adapter.CoroutineViewHolder
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewType
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewTypeDelegateAdapter
import com.kevincianfarini.saddleplayer.util.inflate
import com.kevincianfarini.saddleplayer.util.load
import kotlinx.android.synthetic.main.list_item_artist.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class ArtistDelegateAdapter(private val db: MusicDatabase) : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup): CoroutineViewHolder = ArtistViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        (holder as ArtistViewHolder).bind(item as Artist)
    }

    private inner class ArtistViewHolder(parent: ViewGroup) : CoroutineViewHolder(parent.inflate(R.layout.list_item_artist)) {
        fun bind(artist: Artist) = with(itemView) {
            this@ArtistViewHolder.launch(Dispatchers.Main) {
                val albumCount = this.async(Dispatchers.IO) { db.artistDao.albumCount(artist.id) }
                val trackCount = this.async(Dispatchers.IO) { db.artistDao.trackCount(artist.id) }

                val numAlbums: String = with(albumCount.await()) {
                    context.resources.getQuantityString(R.plurals.num_albums, this, this)
                }

                val numTracks: String = with(trackCount.await()) {
                    context.resources.getQuantityString(R.plurals.num_tracks, this, this)
                }

                artistName.text = artist.name
                artistDetails.text = context.resources.getString(R.string.artist_information, numAlbums, numTracks)
                artistImage.load(artist.imageUrl ?: "", R.drawable.default_artist_image)
            }
        }

    }
}