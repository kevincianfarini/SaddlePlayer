package com.kevincianfarini.saddleplayer.feature.navigation.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.kevincianfarini.saddleplayer.feature.albumlist.view.AlbumListFragment
import com.kevincianfarini.saddleplayer.feature.artistlist.view.ArtistListFragment
import com.kevincianfarini.saddleplayer.feature.navigation.view.TabFragment
import com.kevincianfarini.saddleplayer.feature.tracklist.view.TrackListFragment

class NavigationPageAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    companion object {
        const val NUM_PAGES = 3
    }

    private val pages: List<Fragment> = (0 until NUM_PAGES).map {
        when (it) {
            0 -> ArtistListFragment()
            1 -> AlbumListFragment()
            else -> TrackListFragment()
        } as Fragment
    }

    override fun getItem(position: Int): Fragment = pages[position]

    override fun getCount(): Int = NUM_PAGES

    override fun getPageTitle(position: Int): CharSequence? = (pages[position] as TabFragment).title
}
