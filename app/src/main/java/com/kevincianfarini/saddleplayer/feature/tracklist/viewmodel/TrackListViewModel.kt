package com.kevincianfarini.saddleplayer.feature.tracklist.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import com.kevincianfarini.saddleplayer.data.local.dao.TrackDao
import com.kevincianfarini.saddleplayer.data.local.entity.Track
import com.kevincianfarini.saddleplayer.feature.base.viewmodel.ListViewModel

class TrackListViewModel(private val dao: TrackDao) : ViewModel(), ListViewModel<Track> {

    override val data = LivePagedListBuilder(dao.getAll(), 20).build()
    override val error: MutableLiveData<Throwable> = MutableLiveData()
}