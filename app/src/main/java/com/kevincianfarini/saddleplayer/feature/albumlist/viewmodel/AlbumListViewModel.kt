package com.kevincianfarini.saddleplayer.feature.albumlist.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import com.kevincianfarini.saddleplayer.data.local.dao.AlbumDao
import com.kevincianfarini.saddleplayer.data.local.entity.Album
import com.kevincianfarini.saddleplayer.feature.base.viewmodel.ListViewModel

class AlbumListViewModel(private val dao: AlbumDao) : ViewModel(), ListViewModel<Album> {

    override val data = LivePagedListBuilder(dao.getAll(), 20).build()
    override val error: MutableLiveData<Throwable>  = MutableLiveData()

}