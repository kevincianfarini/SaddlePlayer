package com.kevincianfarini.saddleplayer.feature.navigation

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kevincianfarini.saddleplayer.R
import com.kevincianfarini.saddleplayer.feature.navigation.adapter.NavigationPageAdapter
import com.kevincianfarini.saddleplayer.util.inflate
import kotlinx.android.synthetic.main.fragment_navigation.*

class NavigationFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = container?.inflate(R.layout.fragment_navigation)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pager.apply {
            adapter = NavigationPageAdapter(childFragmentManager)
//            offscreenPageLimit = NavigationPageAdapter.NUM_PAGES
        }

        tabLayout.apply {
            setupWithViewPager(pager)
        }
    }
}
