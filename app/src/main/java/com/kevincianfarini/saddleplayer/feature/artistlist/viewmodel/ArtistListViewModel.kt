package com.kevincianfarini.saddleplayer.feature.artistlist.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.kevincianfarini.saddleplayer.data.local.dao.ArtistDao
import com.kevincianfarini.saddleplayer.data.local.entity.Artist
import com.kevincianfarini.saddleplayer.feature.base.viewmodel.ListViewModel

class ArtistListViewModel(private val dao: ArtistDao) : ViewModel(), ListViewModel<Artist> {

    override val data: LiveData<PagedList<Artist>> = LivePagedListBuilder(dao.getAll(), 20).build()
    override val error: MutableLiveData<Throwable> = MutableLiveData()

}