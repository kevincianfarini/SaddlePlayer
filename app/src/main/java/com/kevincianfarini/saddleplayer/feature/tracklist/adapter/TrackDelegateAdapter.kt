package com.kevincianfarini.saddleplayer.feature.tracklist.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.kevincianfarini.saddleplayer.R
import com.kevincianfarini.saddleplayer.data.local.MusicDatabase
import com.kevincianfarini.saddleplayer.data.local.dao.TrackDao
import com.kevincianfarini.saddleplayer.data.local.entity.Track
import com.kevincianfarini.saddleplayer.feature.base.adapter.CoroutineViewHolder
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewType
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewTypeDelegateAdapter
import com.kevincianfarini.saddleplayer.util.inflate
import com.kevincianfarini.saddleplayer.util.load
import kotlinx.android.synthetic.main.list_item_track.view.*
import kotlinx.coroutines.*


class TrackDelegateAdapter(private val db: MusicDatabase) : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup): CoroutineViewHolder = TrackViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        (holder as TrackViewHolder).bind(item as Track)
    }

    private inner class TrackViewHolder(parent: ViewGroup) : CoroutineViewHolder(parent.inflate(R.layout.list_item_track)) {

        fun bind(track: Track) = with(itemView) {
            this@TrackViewHolder.launch(Dispatchers.Main) {
                val album = this.async(Dispatchers.IO) {
                    db.albumDao.get(track.album)
                }
                trackName.text = track.name
                trackImage.load(album.await().imageUrl ?: "", R.drawable.default_album_art)
            }
        }

    }
}