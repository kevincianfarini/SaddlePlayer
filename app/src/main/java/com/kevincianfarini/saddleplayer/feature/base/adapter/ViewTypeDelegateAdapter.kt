package com.kevincianfarini.saddleplayer.feature.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

interface ViewTypeDelegateAdapter {
    fun onCreateViewHolder(parent: ViewGroup): CoroutineViewHolder
    fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType)
}