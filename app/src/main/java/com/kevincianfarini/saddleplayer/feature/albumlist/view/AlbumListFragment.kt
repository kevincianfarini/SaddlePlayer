package com.kevincianfarini.saddleplayer.feature.albumlist.view

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.View
import com.kevincianfarini.saddleplayer.data.local.entity.Album
import com.kevincianfarini.saddleplayer.di.AppComponent
import com.kevincianfarini.saddleplayer.feature.albumlist.adapter.AlbumDelegateAdapter
import com.kevincianfarini.saddleplayer.feature.albumlist.viewmodel.AlbumListViewModel
import com.kevincianfarini.saddleplayer.feature.base.adapter.LayoutManagerType
import com.kevincianfarini.saddleplayer.feature.base.adapter.ViewTypeConstant
import com.kevincianfarini.saddleplayer.feature.base.view.ListFragment
import com.kevincianfarini.saddleplayer.feature.navigation.view.TabFragment

class AlbumListFragment : ListFragment<Album>(), TabFragment {


    override val layoutManagerType: LayoutManagerType = LayoutManagerType.GRID_2
    override val title: String = "Albums"

    override fun injectDependencies(component: AppComponent) {
        component.inject(this)
    }

}