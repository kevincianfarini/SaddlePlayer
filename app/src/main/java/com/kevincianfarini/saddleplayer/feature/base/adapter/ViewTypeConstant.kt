package com.kevincianfarini.saddleplayer.feature.base.adapter

enum class ViewTypeConstant {

    ARTIST, ALBUM, TRACK

}