package com.kevincianfarini.saddleplayer.feature.base.adapter

interface ViewType {

    val viewType: ViewTypeConstant

    /**
     * Used for paging differential adapter
     */
    fun areItemsSame(other: ViewType?): Boolean

}