package com.kevincianfarini.saddleplayer.feature.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import kotlin.coroutines.CoroutineContext

abstract class CoroutineViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), CoroutineScope {

    private val job = SupervisorJob()
    override val coroutineContext: CoroutineContext = Dispatchers.Main + job

    fun onRecycled() {
        coroutineContext.cancelChildren()
    }

}