package com.kevincianfarini.saddleplayer.feature.base.adapter

enum class LayoutManagerType {

    LINEAR, GRID_2

}