package com.kevincianfarini.saddleplayer.feature.main

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import com.kevincianfarini.saddleplayer.R
import com.kevincianfarini.saddleplayer.SaddleApplication
import com.kevincianfarini.saddleplayer.feature.navigation.NavigationFragment
import kotlinx.coroutines.*

import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class MainActivity : AppCompatActivity(), CoroutineScope {

    @Inject lateinit var viewModel: MainViewModel
    private val job = SupervisorJob()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (this.application as SaddleApplication).appComponent.inject(this)
        setFragment(NavigationFragment())
        viewModel.error.observe(this, Observer {
            showErrorSnackbar(it?.message ?: "Error without message.")
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        this.job.cancelChildren()
    }

    fun setFragment(fragment: Fragment, addToBackStack: Boolean = false) {
        with(supportFragmentManager.beginTransaction()) {
            this.replace(R.id.content, fragment)
            if (addToBackStack) {
                this.addToBackStack(null)
            }
            this.commit()
        }
    }

    fun refreshData(lastUpdated: Long? = System.currentTimeMillis(), finished: () -> Unit) = launch(Dispatchers.IO) {
        viewModel.refreshData(lastUpdated)
        withContext(coroutineContext) { finished() }
    }

    private fun showErrorSnackbar(message: String) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show()
    }
}
