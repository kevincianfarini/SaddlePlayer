package com.kevincianfarini.saddleplayer.feature.navigation.view

interface TabFragment {

    val title: String

}